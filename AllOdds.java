import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by Packard bell on 9/21/2015.
 */
public class AllOdds
{
    public static void main(String[] args)
    {
        int number = Integer.parseInt(JOptionPane.showInputDialog(null, "enter number"));
        ArrayList<Integer> oddNumberLists = new ArrayList<Integer>();
        oddNumberLists = listOfOddNumber(number);
        System.out.print(oddNumberLists);

    }
    public static boolean isOddNumber(int number)
    {
        boolean isOdd = false;
        for(int x = 3; x <= number ; x=x+3)
        {
            if(number % x == 0)
            {
                isOdd = true;
            }
        }
        return isOdd;
    }
    public static ArrayList<Integer> listOfOddNumber(int number)
    {
        ArrayList<Integer> oddNumberList = new ArrayList<Integer>();
        for(int x = 1; x <= number;x++)
        {
            if(isOddNumber(x))
            {
                oddNumberList.add(x);
            }
        }
        return oddNumberList;
    }
}
