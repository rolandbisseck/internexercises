/**
 * Created by Packard bell on 9/22/2015.
 */
public class CallingMethod
{

    public static void main (String[] args)
    {
        //Calling a method that requires no arguments
            noArguments();

        //Calling a method with a fixed number of arguments
            setFixedNumberOfArguments(2, 50.90);

       // Calling a method with optional arguments
        setOptionalArgument(true);

        //Calling a method with a variable number of arguments
        setVariableNumberOfArgument("bisseck", 24, 100000);

       // Obtaining the return value of a method
         int number = getNumber();

        //Private
        privateMethod();

        //protected
        protectedMethod();

        // public methods


    }
    //this method requires no arguments.
    public static void noArguments()
    {

    }
    //this method requires a fixed number of argument.passed by value
    public static void setFixedNumberOfArguments(int number, double price)
    {

    }
    //this method required an optional argument; passed by value
    public static void setOptionalArgument(boolean isTrue)
    {

    }
    //Calling a method with a variable number of arguments, passed by value
    public static void setVariableNumberOfArgument(String name, int number, double price)
    {

    }
    //Obtaining the return value of a method, passed by reference
    public static int getNumber()
    {
        int number = 0;

        return number;
    }
    private static void privateMethod()
    {

    }
    protected static double protectedMethod()
    {
        double price = 0.0;
        return price;
    }

}
