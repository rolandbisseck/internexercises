/**
 * Created by Packard bell on 9/21/2015.
 */
public class FindTheMaximum
{
    public static void main(String[] args)
    {
        int[] numbers = {23,67,8,2,12,156};
        int high = determineHighest(numbers);
        System.out.println(high);
    }
    public static int determineHighest(int[] numberArray)
    {
        int highest = numberArray[0];
        for(int x = 0; x < numberArray.length; x++)
        {
            if(numberArray[x] > highest)
            {
                highest = numberArray[x];
            }
        }
        return highest;
    }
}
