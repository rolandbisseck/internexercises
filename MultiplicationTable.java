/**
 * Created by Packard bell on 9/21/2015.
 */
public class MultiplicationTable
{
    public static void main(String[] args)
    {
        for (int x = 1; x <= 12; x++)
        {
            for (int y = 1; y <= 12; y++)
            {
                System.out.print(x + "x" + y + "=" + (x * y) + "\n");
            }
            System.out.println();
        }
    }

}
