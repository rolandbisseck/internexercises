/**
 * Created by Packard bell on 9/17/2015.
 */
public class SumOfNumberInAnArray
{
    public static void main(String[] args)
    {
        int []numbers = {12, 34, 89, 90, 32, 1, 2, 5};
        int sum = 0;
        for(int z : numbers)
        {
            sum += z ;

            System.out.println(z + "\n" + "------" + "\n" + sum);
        }

    }
}
