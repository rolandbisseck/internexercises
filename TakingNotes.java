import java.io.IOException;
import java.io.*;
import java.util.*;
/**
 * Created by Packard bell on 9/22/2015.
 */
public class TakingNotes
{
    public static void main (String[] args)
    {
        File file = new File("C:\\Users\\Packard bell\\Documents\\angrams\\note.txt");

        if(file.exists())
        {
            ArrayList<String> myList = new ArrayList<String>();
            try
            {
                myList = readNote(file);
                System.out.println(myList);
                if(file.toString().isEmpty() == false)
                {
                    writeToFile(file);

                }
            }
            catch(Exception er)
            {
                er.getMessage();
            }

        }
        else
        {
            file = new File("C:\\Users\\Packard bell\\Documents\\angrams\\note.txt");
        }
    }
    public static ArrayList<String> readNote(File file) throws IOException
    {
        ArrayList<String> wordList = new ArrayList<String>();
        BufferedReader buffer = new BufferedReader(new FileReader(file));
        String record = "";
        while((record = buffer.readLine()) != null)
        {
            wordList.add(record);
        }
        buffer.close();
        return wordList;
    }
    public static void writeToFile(File file)throws IOException
    {
        BufferedWriter buffer = new BufferedWriter(new FileWriter(file,true));
        String record = "";
        Date date = new Date();
        String todayDate = date.toString();

        record = todayDate;
        buffer.write(record);
        buffer.newLine();
        buffer.close();
    }
}
