/**
 * Created by Packard bell on 9/22/2015.
 */
import java.io.*;
import java.util.ArrayList;

public class WalkDirectoryTree
{
    public static void main(String[] args)
    {
        File directory = new File("C:\\Users\\Packard bell\\Documents\\angrams");
        ArrayList<String> fileList = new ArrayList<String>();
        try
        {


             fileList= checkAndReadDirectory(directory);
            for(int x=0; x < fileList.size(); x++)
            {
                if(fileList.get(x).endsWith(".txt"))
                {
                    System.out.println(fileList.get(x));
                }
            }

        }
        catch(Exception er)
        {
            er.getMessage();
        }
    }
    public static ArrayList<String> checkAndReadDirectory(File file)throws IOException
    {
        BufferedReader buffer = new BufferedReader(new FileReader(file));

        ArrayList<String> listFile = new ArrayList<String>();
        String record = "";
        if(file.isDirectory())
        {
            while((record = buffer.readLine()) != null)
            {
                listFile.add(record);
            }
            buffer.close();
        }
        return listFile;
    }

}
